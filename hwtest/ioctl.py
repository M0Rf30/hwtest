import fcntl

_EVIOCGNAME = 0x82004506


def EVIOCGNAME(device_node):
    f = open(device_node, 'rb')
    try:
        buffer = bytearray(512)
        fcntl.ioctl(f, _EVIOCGNAME, buffer, True)
        name = buffer[:buffer.find(b"\0")].decode("utf-8")
        return name
    finally:
        f.close()
